# -*- coding: utf-8 -*-
import logging
import hashlib

import sqlalchemy as sa

from .common import Common
from .meta import Base

log = logging.getLogger(__name__)


class FileContent(Base, Common):
    __tablename__ = 'filecontent'

    id = sa.Column(sa.Integer, primary_key=True)
    signature = sa.Column(sa.Text, index=True, unique=True)
    size = sa.Column(sa.Integer)

    @classmethod
    def from_signature(cls, dbsession, signature):
        return dbsession.query(cls).filter(cls.signature == signature).first()

    @staticmethod
    def hash(content):
        m = hashlib.sha256()
        m.update(content)
        return m.hexdigest()
